[break-after--]
# Lieu de partage de cette curiosité insatiable pour l'art.


[break-after--]
## L'art vivant comme véhicule de transformation des êtres.

<!-- Page 2 : Photo du foyer avec du public. -->

<!-- ![](photo-ambiance-foyer-balsa-credit-fanny-arvieu-low.jpg) -->



<!-- Page 3 : Edito -->



[edito--]
### L’art vivant comme véhicule </br>de transformation des êtres

Depuis plusieurs mois, suite à la pandémie du Covid-19, nous avons été privé·e·s de nos besoins fondamentaux, séparé·e·s de nos familles, de nos ami·e·s, de nos collègues. Le confinement a largement confirmé, voire accentué la fracture de notre société. Dans un contexte inégalitaire exacerbé par une économie en léthargie, il est vrai que certain·e·s se sont démené·e·s, parfois jusqu'à l'épuisement, afin de pallier aux injustices sociales.

Du courage, nous n'en avons pas manqué&thinsp;! De la créativité, non plus.
Et, malgré cela, la culture s'est vue bafouée, honnie, une fois de plus, une fois de trop. Dans la gestion de cette crise inédite, elle fut la grande oubliée des phases liées au déconfinement, reléguée après la relance des centres commerciaux, après les kayakistes, après les compagnies aériennes…
Dès lors, nos accès à la connaissance, à l'éducation, à l'art, ont été exclus du vivant, limités à l'utilisation exclusive des réseaux sociaux.

Et, à présent, comment penser l'après&thinsp;? </br>Comment veiller à ce que cette crise ne soit pas la prémisse d'une crise plus substantielle&thinsp;? Comment réinventer, revivifier notre quotidien dévitalisé&thinsp;? Comment cesser de perdre autant de temps et d'énergie à justifier notre place et notre
utilité&thinsp;?

Un trauma est en nous. La confiance en ce temps passé qui structurait nos jours s'est altérée. Cependant, nous pouvons saisir ce trouble, à bras-le-corps. Tout en préservant notre vigilance, nous pouvons nous manifester, plus physiquement que jamais. Être habité·e·s. La création, les arts vivants sont des générateurs infinis de possibles et développent cet éveil constant de nos intériorités. Plus que jamais créons, inventons, réalisons des choix plus conformes à nos valeurs, défendons nos idées, donnons-leur une chance de se déployer, trouvons ce que chacun·e peut apporter d'unique au sein de nos collectivités. Et surtout ménageons-nous du temps pour dessiner un futur à notre image, à taille humaine. La Balsamine vous invite à travers ce livret à mieux la connaître. Les œuvres qui naissent en nos murs, tels des biens de première nécessité, doivent être accessibles à toutes et tous, nous y veillons, formidables passerelles entre les êtres et vecteurs de
transformation.
[--edito]


Monica Gomes</br>Direction générale et artistique de la Balsamine</br>Juin 2020




<!-- Page 4-5 -->
[break-before--]
### La Balsamine, ses missions

L'accompagnement des artistes est au centre de la philosophie du lieu. À la Balsamine, nous épaulons les artistes qui développent un certain type d'écriture : celle qui part du plateau et se décline sous toutes les formes, qu'elle soit sonore, visuelle, plastique, corporelle et, naturellement, textuelle. Avec de nombreuses résidences de recherche et de création, nous consolidons la pratique de jeunes artistes de la Fédération Wallonie-Bruxelles, favorisons l'émergence de formes artistiques contemporaines tout en permettant à un large public de partager leur recherche par le biais d'ateliers et d'animations.
[--break-before]


### Un rayonnement national </br>et international

Vu la difficulté croissante à promouvoir des artistes émergent·e·s, à défendre des artistes qui proposent des œuvres exigeantes et singulières, il n'est pas évident de leur offrir nécessairement un rayonnement immédiat. Cependant, notre conviction porte ses fruits puisque, force est de constater, que le succès public et professionnel est au rendez-vous.

### Un ancrage territorial

L'une des priorités de notre théâtre est l'ouverture aux différents tissus sociaux environnants. Partager avec les habitant.e.s de notre commune, Schaerbeek, et notre quartier proche, Dailly, la vitalité artistique quotidienne du lieu. Pour ce faire, nous travaillons en étroite synergie avec des associations et des écoles partenaires, en menant des actions ponctuelles et récurrentes : participation à la
brocante de quartier, visites du théâtre, animations autour des
spectacles, stage de pratique théâtrale pour adolescent·e·s, gestion d'un ciné́-club avec la programmation des «Balsatoiles»,...


[break-before--]
### Un lieu d'initiation au théâtre contemporain

Chaque saison, plus de deux mille personnes (tous âges confondus) bénéficient, gratuitement, d'ateliers et d'animations, ainsi que de rencontres avec les artistes.

La politique culturelle du théâtre la Balsamine permet d'offrir, systématiquement, un accompagnement pédagogique (sorties scolaires en groupes) et ce, dans l'intention d'optimaliser la rencontre des publics avec l'œuvre en devenir. Cet accompagnement est dispensé avant, et/ou, après la représentation choisie, soit au sein des infrastructures
scolaires, soit au sein du théâtre. L'encadrement est assuré par les initiateur·trice·s du projet, en collaboration étroite, avec la médiatrice du théâtre : Noemi Tiberghien, romaniste et comédienne.
[--break-before]


### Des partenaires avec lesquels nous construisons des temps forts de la vie culturelle et artistique belge

Ars musica, la Chambre des Théâtres pour l’Enfance et la Jeunesse (CTEJ), Brussels, dance!, Kunstenfestivaldesarts, United Stages, Festival UP, Bâtard Festival.

### Échanges de résidences artistiques

Depuis un an, nous nous associons avec le Théâtre de Poche de Hédé-Bazouges (Bretagne - FR) et le Théâtre du Grütli (CH) dans le cadre d'un échange de résidences d'artistes.

### Nos valeurs

La créativité, la curiosité, l'ouverture, le partage, la collaboration, l'engagement, le soutien, l'écoute, la convivialité, le respect de soi, des autres, des ressources.


<!-- Pages 6-7 -->

### Nous et Vous, « Nvous »

Au sein de cette période si particulière que nous traversons tou·te·s, notre façon de communiquer vers vous, avec vous, va quelque peu changer et, surtout, poursuivre son involution. Ce terme, emprunté au philosophe Gilles Deleuze, signifie évoluer autrement, en toute sobriété. Involuer, ce n'est ni progresser ni régresser, c'est être entre, au milieu, adjacent. Nous serons ce trait d'union qui vous relie simplement aux artistes.

Exit le programme de saison. Les incertitudes pesant sur le maintien de l'ensemble de notre programmation 20/21 telle que nous l'avons construite, nous poussent à ne pas imprimer de programme de saison, cette année. Par souci d'économie certes, mais aussi par respect des ressources.


### Le support papier, </br>au plus près des artistes

L'impression des flyers des spectacles sera maintenue mais dans un volume restreint, au plus proche de la demande réelle. Leur conception sera réalisée, tout au long de la saison, en collaboration étroite avec chaque artiste. Cela vous permettra d'entrer dans une projection visuelle de leur création et d'être au plus près de leur imaginaire.  Tout en souhaitant réduire de manière significative nos supports de communication papier, nous tenons à ne pas nous inscrire dans la fracture numérique. Si votre bonne information requiert le support papier, signifiez-le-nous !

Si vous souhaitez les recevoir par voie postale, n'hésitez pas à nous transmettre votre destination, via l'adresse [info@balsamine.be](mailto:info@balsamine.be)


### Vers une communication web</br>plus optimale

La saison 2020-2021, augmentée de certains reports de la saison 2019-2020, sera dévoilée et détaillée sur notre site le 22 juin. Un site qui sera plus dynamique et vous informera de tout ce qui grandit et murit au sein de la Balsamine.


![lucarne](lucarne.svg)
<p id="new"><span class="up">Nouveauté</span> : Une chaîne Balsa verra le jour sur le web également en juin. Ce média vous permettra d'être en lien avec la vie de la Balsa et les artistes qui composent notre saison. C'est pour nous une manière de rester connecté·e·s avec vous au-delà des soirs de représentations.</p>

<span class="up">lucarne.balsamine.be</span>

### Le pass Balsa - toujours et encore

![pass](pass.svg)

Notre fameux abonnement théâtre au prix démocratique de <span class="up">25€ pour 7 spectacles</span>, est toujours d'actualité. Comme chaque année, il donne accès à la presque totalité de notre saison, invitant à naviguer entre les formes et les expériences, du théâtre à la danse, en passant par la performance. La seule différence c'est qu'il sera dématérialisé, pas de petite carte à poinçonner à la billetterie. Ne vous inquiétez pas, si vous souhaitez l'offrir, on vous concocte un chouette petit support, idéal pour gâter vos proches&thinsp;!

En cette période difficile pour le secteur culturel, acheter ou offrir un pass est tout d'abord un geste de fidélité à la Balsamine mais également un soutien. C'est faire preuve de curiosité, c'est se lancer dans l'aventure Balsa et vivre l'expérience Balsa&thinsp;! Mais c'est aussi participer à la pérennité de nos fondamentaux que sont l'échange, le partage, le dialogue, la création et l'enjeu artistique et expérimental.


<!-- ##### 40 -->

![anni](balsanniversaire.svg)


### 40 ans déjà&thinsp;!

En 1981, la Balsamine prenait racine dans un site abandonné, la Caserne Dailly. Le gigantisme du site et sa diversité invitent Martine Wijckaert à y poser ses valises. «&thinsp;L'époque Balsa&thinsp;» commence véritablement en 1981 et se fonde sur l'exploitation du site au titre de décor naturel mouvant. C'est à cette même époque que Martine Wijckaert crée "La pilule
verte" d'après Witkiewicz.



![passe](photo-amphi-daniele-pierre-print.jpg)

![present](amphi-balsa-hichem-print.jpg)



Nous voici, presque 40 ans plus tard, en pleine préparation de ce Balsanniversaire, que nous fêterons en juin 2021. Pour amorcer cet événement, nous réalisons une petite enquête et sollicitons votre mémoire avec cette question :

![love](love.svg)

<div class="frame">Quel est le spectacle qui vous a le plus marqué à la Balsa&thinsp;? Votre coup de cœur&thinsp;? </br>Vous pouvez nous en donner plusieurs, </br>et même partager avec nous votre top 3, 5 ou 10&thinsp;! Tout est ok&thinsp;! N'hésitez pas à nous envoyer vos témoignages à l'adresse <a src=info@balsamine.be>info@balsamine.be</a>
</div>



<!-- ### Petit calendrier de la saison future →


#### 137 façons de mourir
<span class="artiste">Virginie Strub/ Kirsh Cie</span>
du 29 /09 au 9/10
Création théâtre


#### Au pied des Montagnes
<span class="artiste">Une Tribu Collectif</span>
du 7 au 17/11
Création théâtre d'ombres et de marionnettes

#### Les lianes
<span class="artiste">Françoise Berlanger</span>
du 10 au 15/11
Création
Théâtre / Musique

#### XX TIME
du 30/09 au 07/12

#### In my big fireworks I play to you the final bouquet
<span class="artiste">Les sœurs h</span>
Concert performé

#### Forces
<span class="artiste">Mannès | Turine | Lemaître</span>
Danse

#### Tout doit disparaitre
<span class="artiste">Ophélie Mac</span>
Performance démo

#### ¾ Face
<span class="artiste">Loraine Dambermont</span>
Création danse

#### Ouvrir la voix
<span class="artiste">Amandine Gay</span>
Film documentaire

#### Serial Lovers
<span class="artiste">Bibi Lona (aka Barbara Mavro Thalassitis)</span>
Concert

#### Todos Caeran
<span class="artiste">Le Colonel Astral</span>
du 3 au 12/02
Création théâtre

#### Auteur inconnu
<span class="artiste">Anaïs Moreau</span>
du 2 au 6/03
Création théâtre

#### Le Nid
<span class="artiste">Thi-Mai Nguyen</span>
du 30/03 au 3/04
Création danse

#### Volvox
<span class="artiste">noch company I oriane varak</span>
du 30/03 au 3/04
Création danse

#### Forets paisibles
<span class="artiste">Martine Wijckaert</span>
du 15 au 25/06
Création théâtre

#### Balsanniversaire
du 15 au 25/06
Fête de clôture le 25/06




Théâtre la Balsamine

Avenue Félix Marchal, 1

1030 Schaerbeek

[www.balsamine.be](http://www.balsamine.be)

Réservation : 02 735 64 68

Administration : 02732 96 18



![](logo-story-screen.png)

facebook.com/balsamine.theatre

instagram.com/balsamine1030

twitter.com/balsatheatre -->
